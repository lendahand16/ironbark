
# The Structure of the Ironbark Server

## HTTP/HTTPS Server
```
+ Trunk:
    The core web server of the program.

+ Bark:
    A folder directory that appears under a different subdomain.

+ Branch
    A subdomain that acts as a proxy to another app.
```
## Application
```
+ index(.ts):
    Handles launching the application.
    Allows loading of modules thorugh passing.

+ IronbarkFoundation(.ts):
    Generate text strings/error messages
    Debugging stuffs
    Core components of the application.

+ Ironbark(.ts):
    Http servers handling

+ IronbarkData(.ts):
    Very large data componets of the application such as images in base64.
```
## Project
```
build/ (Bundled with full program)
 |-bin/
  |-node_modules/
  |-Ironbark.js
  |-package-lock;json
  |-package.json
 |-config/
 |-ironbark-modules/
  |-myModuleFolder/
   |-index.js
  |-myModuleFile.js
 |-ironbark-www/
 |-Launch.sh
```