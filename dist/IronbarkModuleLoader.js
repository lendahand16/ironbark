"use strict";
//@ts-check
Object.defineProperty(exports, "__esModule", { value: true });
console.log("Module Loader beginning");
var FS = require("fs");
//import myMod = require("../ironbark-modules/ironbark-my1");
var MODULES_PATH = "./ironbark-modules/";
var modulesToLoad = FS.readdirSync(MODULES_PATH);
//console.log(modulesToLoad);
//console.log("+", require.resolve.paths("../ironbark-modules/Ironbark.ts"))
var modules = [];
for (var _i = 0, modulesToLoad_1 = modulesToLoad; _i < modulesToLoad_1.length; _i++) {
    var filename = modulesToLoad_1[_i];
    var name = filename.split(".").splice(0, filename.split(".").length - 1).join("");
    var extension = filename.split(".")[filename.split(".").length - 1];
    if (extension == "js" && name != "_template") {
        console.log("Loading Module: " + name);
        modules.push(require("../" + MODULES_PATH + name));
    }
}
console.log(modules);
//console.log(FS.readdirSync(MODULES_PATH));
console.log("Module Loader done");
