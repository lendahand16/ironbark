import IBFoundationImport = require("./IronbarkFoundation");
export declare const IBFoundation: typeof IBFoundationImport.IronbarkFoundation;
export interface TrunkSettings {
    serverRoot?: string;
    branchServers?: Branch[];
    barkServers?: Bark[];
    defaultRedirect?: Bark;
}
export declare class Trunk {
    private httpServer;
    private serverPort;
    private serverRoot;
    readonly barkServers: Bark[];
    readonly port: number;
    constructor(serverPort: number, serverOptions?: TrunkSettings);
    start(): void;
    stop(): void;
    private test();
    private respondToRequest(request, response);
    private respondToHEAD(request, response);
    private respondToGET(request, response);
    private respondToPOST(request, response);
}
export declare class Bark {
}
export declare class Branch {
}
