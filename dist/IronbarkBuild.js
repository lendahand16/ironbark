"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var fileString = "";
var myRegex = /(\/\/ IBBuild::Start)(.*?)(IBBuild::End)/gms;
var myArray;
var MODULES_PATH = "./";
var modulesToLoad = fs.readdirSync(MODULES_PATH);
var files = [];
for (var _i = 0, modulesToLoad_1 = modulesToLoad; _i < modulesToLoad_1.length; _i++) {
    var filename = modulesToLoad_1[_i];
    var name = filename.split(".").splice(0, filename.split(".").length - 1).join(".");
    var extension = filename.split(".")[filename.split(".").length - 1];
    if (extension == "ts" && name != "IronbarkBuild") {
        console.log(name + extension);
        fs.readFile(name + "." + extension, function (err, data) {
            fileString = data.toString();
            while ((myArray = myRegex.exec(fileString)) !== null) {
                console.log(myArray[0]);
            }
        });
    }
}
