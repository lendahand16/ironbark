/// <reference types="node" />
import HTTP = require("http");
import IBDataImport = require("./IronbarkData");
export declare const IBData: typeof IBDataImport.IronbarkData;
export declare class IronbarkFoundation {
    static DEBUG_LEVEL: number;
    /**
     * A function that serves as a debugging tool for the Ironbark server console.
     * This tries to emulate the default console.log() method.
     * @param level Provides the debug level at which the message is printed.
     * @param message The text to be printed if the level parameter is less than or equal to the debug level.
     *
     * @returns {boolean} Returns whether or not the message was printed.
     */
    static log(level: number, message: any, ...optionalParams: any[]): boolean;
    static readonly mimeTypes: {
        [key: string]: string;
    };
    static readonly statusMessages: {
        [key: number]: string;
    };
    static serveErrorPage(code: number | undefined, request: HTTP.IncomingMessage, response: HTTP.ServerResponse, onlyHeader?: boolean): void;
}
