//
// IBBuildModule::index
//

const PORT: number = Number(process.env.PORT) || 80;
IBFoundation.DEBUG_LEVEL = 2;
//var server = new Ironbark.IronbarkHTTPServer( PORT );
// need to input domain/hostname so subdomains can be used
var server = new Ironbark.Trunk( PORT );
server.start();

//
// IBBuildModule::Ironbark
//


import HTTP = require("http");      // NodeJS HTTP Module
import HTTPS = require("https");    // NodeJS HTTPS Module
import FS = require("fs");          // NodeJS File System Module
import SQLi3 = require("sqlite3");  // NodeJS SQLite3 Module




interface TrunkSettings {
    serverRoot?: string;
    branchServers?: Branch[];
    barkServers?: Bark[];
    defaultRedirect?: Bark;
}

class Configuration {
    // Recursively filter through each folder from the root and find a '.ironbark' file for configuration.
    // Every '.ironbark' becomes a 'root' for the path heierachy.
    constructor (  ) {

    }

}

/*
IronbarkWebServer
|-bin
|-docs
|-modules
|-ironbark-www
 |-Admin
  |-admin1
  |-sysadmin
 |-Users
  |-2020
   |-LENE08
    |-.ironbark
    |-Data
     |-MyWebsite!
  |-2021
  |-2022

*/

class Trunk {

    private httpServer: HTTP.Server;
    private serverPort: number = 80;
    private serverRoot: string;
    public readonly barkServers: Bark[] = [];
    public get port (): number { return this.serverPort };

    constructor ( serverPort: number, serverOptions?: TrunkSettings) {
        this.serverPort = serverPort;
        this.httpServer = HTTP.createServer( (req, res)=>{ this.respondToRequest(req,res); });
        this.serverRoot = "./ironbark-www" ;//|| process.execPath;
    }

    public start (): void {
        this.httpServer.listen( this.serverPort );
        IBFoundation.log(0, "Started Server on PORT:",this.serverPort);
    }

    public stop (): void {
        this.httpServer.close(()=>{
            IBFoundation.log(0, "Closed Server on PORT:",this.serverPort);
        });
    }

    private test(): void {
        //FS.lstatSyn
    }
    

    private respondToRequest ( request: HTTP.IncomingMessage, response: HTTP.ServerResponse, ) {
        
        //IronbarkFoundation.log(2, request.method,">>",request.url);
        IBFoundation.log(2, request.headers);
        switch (request.method) {
            case "HEAD":
                this.respondToHEAD( request, response );
                break;
            case "GET":
                this.respondToGET( request, response );
                break;
            case "POST":
                this.respondToPOST( request, response );
                break;
            default:
                response.writeHead(400, request.method+" request method is not supported.", { "Allow": "GET, POST, HEAD" });
                break;
        }

    }

    private respondToHEAD ( request: HTTP.IncomingMessage, response: HTTP.ServerResponse ) {
        response.writeHead(200);
        response.end();
    }

    private respondToGET ( request: HTTP.IncomingMessage, response: HTTP.ServerResponse ) {

        var filePath = "."+request.url;
        var filePath = this.serverRoot+request.url;
        if (filePath[filePath.length-1]=="/") {
            filePath = this.serverRoot+request.url+"index.html";
        }
        var fileExtenstion = "."+filePath.split(".").reverse()[0];
        var contentType = IBFoundation.mimeTypes[fileExtenstion] || "application/octet-stream";
        
        FS.readFile(filePath, function (error, content) {
            if (error) {
                if (error.code == "ENOENT") {
                    IBFoundation.serveErrorPage(404, request, response);
                } else if (error.code == "EISDIR") {
                    IBFoundation.serveErrorPage(404, request, response);
                }
                IBFoundation.log(2, "ERR", error);
                return;
            }
            response.writeHead(200, { 'Content-Type': contentType });
            response.write(content, ()=>{
                response.end();
            });
        });
    }

    private respondToPOST ( request: HTTP.IncomingMessage, response: HTTP.ServerResponse ) {
        null;
        SQLi3;
    }

}
class Bark {


}
class Branch {

}
/*
var myTrunk = {
    serverPort: 8080, // The main server port at "example.com:8080"
    defaultRedirect: "sub.example.com" || null,
    barkServers: [
        {
            name: "bark1",
            subdomain: "sub",
            rootDirectory: "/sub/",
            barkServers: [
                {}
            ]
        }
    ],  // Subdomains that do not require their own webserver "sub.example.com:8080"
    branchServers: [] // Subdomains that are really other web applications  "app.example.com:8080" would forward to non-public "app:8081"
}

myTrunk;*/


//
// IBBuildModule::IronbarkData
//

// IBBuild::Enable

namespace IBData {

    export const websiteIcon = {
        "header": "data:image/png;base64,",
        "data": "iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAMAAABrrFhUAAAABGdBTUEAALGPC/xhBQAAAwBQTFRFAAAA7nYy63oz63s06nwz6n007Hky7Ho07Hw07n8/8m4x9mox9Gwx+WIx+GUx+Ggx+W4+8XEy8HQy8HU09XU+8no/8Xw/9Hg+8nxA9X5L+XpL+H1M3pY525o52p063Jg53Jw6z688w7w8xrk8xLw8yrU8zbE9zLQ8yLg91qI616M81qQ71aY80as70qo80K081ag71Kg82KE62KM80LA954I05oU144435Ik25Iw34o445Ys45Iw46IE06IQ14ZE44JQ5359Fyr5JzLtJzL1J165I2qZH3KJG2atI36hT3K9U0rVJ0r5V1blV2rFU2bVV3rdg27ph3r9t6opB6I1C7otN7Y5N5pFD5pRE45lF4Z1G5JhF6ZVP5p1R65xb8oVM9IFL845Z9YlY+IVY8ZBZ755n+4pk+Y1k9pFk9JRl8Zlm8Jxm+JBk95tx9pxy+Jlx4KZS5KJS5qVd4KhS4q5f5qhe6aJd4LJf7qFn66Zo7aJo7KRo6qpq6K1q47Vr5rFr5LRr4rhs4L1t57l35L1467N26bd37bB29KBy96Z/9qh/+qJ/+KR+x8BJy8VVzsJVzMRV0sVi0Mhi0s1t1slt1slu1ctt1ctu1Mxt2sVt3cFt2s5619F54cR577eC9qmA/KeL+6uL+6uM+a2L+K6M/KmL8LGB87eN9bOM9LWM8bqN9rqZ9b2Z+bWY/LKY+LiY+r6l2tWG3dKF3NSG3tmS5ceF4M+G5ciF6MCE7sGP7sKQ6cmQ78qc7c2c4NeS5NKS4t2e5dqe6tGd6dSe7NCd8sKa8cab8cec9MCa7tWp6tyq9Mqn8s6o+MGl/Mey+smy+Myy/c6+8dGo9tCz9dO09NS08ti18Ny1+9K//NG/5uCq6eW27eG299nA9d3B+NfA+t3M/NvL/NrM/NzM7ujC7OrD8+HB8eXC9ODB9uXN8+vO8ezO8O7O9OnO+OHM+OTN++fZ/eLY/OTY+erZ+Oza9PLb/uvl/e3l+vLm+PXn/PDm/fby+/vz/Pny/v7+AAAAxZ2G/QAAAQB0Uk5T////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////AFP3ByUAAAAJcEhZcwAADsIAAA7CARUoSoAAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMjHxIGmVAAAbVElEQVR4Xs2dDdxfZVnH3XieYXv20hZQJnseF7AxWVtbDDba5rKUytTKSjGLiQabKSC9appYlmkYlb2ZvUtvAo3KCtMVUmIvEqapJGUrKoMNKpDYGs+fruu6f/e5X885933Ofc6f74dNePb8/+f+/q7rvs99zv/s8UmPT4NJFPzhuIwaAERbwbePwlgBQC0DvHBoRggAQp3AWwzIwAHAoxd4q4EYMgAIFABvOASDBYChFwNvW5xhAsCgC4M3L8sAAWC4CfzbUfxLMjhEQYoHgJEmcPRnzjnnnSfwH8ngMMUoGwAGmcLi758jfGARX0gHBytDyQAwviQ+8BXK/5xzvvYf8aUMcMASlAsAY0vi6PWwZza+4xF8OQMctD+lAsC4kjjx01AXNm7csOGP8udBqQjKBIAxJbH4p+dCXSB94jkdloIyEZQIAONJ41NfCXNhI9dfeO5n8A05YAB96B8AxpLG0Z88162/9ifekX1KJDCI7vQNAONI48Q7ST8yAcD6P+swD/pG0DMADCKJxdvOJWBObPT8N6zf+py/xvfmgKF0pFcAGEEa9zzT0bfnv2L9VuK59+Pbc8BwOtEnABw+iWM/e5bn78x/QvyJXxh3KegeAA6dxOIfnMX+TQ2wXgewdeufj3lK7BwADpzC4m3PFH+YC67++Rs2QJ7YsuX5HXbHXSPoGAAOmsQ91591rlv/YPmz608BbNny9Q/gxRlgaJl0CwCHTOGRnzuLCPof4hq4M+y/Zcu2X8pfCjC4PLoEgOOl8Nh7WD8IwJ//cCeUPgWw7VkdlgIMMIcOAeBgKXyKJn+gT/ZuA1j+OoBtzPPyd8cYYgb5AeBQCRy7vtL3FkCYK/z5r/23bd78g9lLAQaZTm4AOE4CJ35e9MP57+//6vzPO2/z5s1/mD0PMNBUMgPAQdrRkz+y/qXWn/3PO++rbsc7JoOhJpIXAA7Rzj1q8hOev6dv+29BAEaf/Omfr8mdBxhsGlkB4ACtHPsp2LfXH/LEFhWA6PP8F39m06Y3HscbJ4LhJpETAN6+jRO/CHmC9d0FAOaK9VutBvhiU3/Lf/MmYvv78pYCDDiFjADw5i08dhvcGb//Pf/zrQZQ+noCiD8nsJkaYNP27duf/Vd4/zQw5ATSA8Bbt3DPT5yt5fmf9P439oSqv/xD+pLA9u+9D4dIAoNuJzkAvHEzj7xd6xNyAQB1BcQV9vLvrf8Cl//pmzZ9KcsL82/KWQow7FZSA8DbNnLi984+2wTgtT/VH+YgPP95+oRqf8U8cThnKcDIW0gLAG/ZzG1vs/QJL4CG879aALW/Ov9zBNQAVQDsPz9/8SdwsBQw+GaKBfDvP07ltyeAX//a+R/pf9EP6i98V8ZSgNE3khQA3q+BE29ne7sB3ACC8psAtL4KQPlLAFH/+fl1tybPAwy/kZQA8Hb1PPaeL3LLH7Z/bQDwd+qvA3g6EoC6sG7duouTlwIINJEQAN6snkfeFvFvnP/2/ifmL/p0/lP+Tv2JhYWrceRWoNBAewB4qwaOiX9jA8Bc0TL/qfybZf2L+y8wOHI7kKinNQC8URPHIvWvAqDdX+r1ry4//Ub+23UDwJ3R/ukBtCbQFgDeppFIAEqeYH+Ia4IF0OizP7cAl1/PALgzqv/zAmhLoCUAvEkzFADUveor0vqf5KsGMPY1/jkBtCTQHADeooVjlb+6/d04/xvXf9UBUv9o/2v/rACaE2gMAG/QhgkgsvzZAZx/ftD/yt6sf7wA1tYf9gSOnAh0ohQJAPJEa/0hH60/978sALCvrf/CmThyItCJ0hQAXt6KCSD0twOwl3+VALZ/cgOMy0+/q/2/ygDuDOtXAZyZGUBTAg0B4MXtqAD4+t/3h7kG6oQUn1D6Cmp+Nf9FnoA74/T/mdkBNCRQHwBemoDuAPa3A/D6P7z95fc//RbvfwoA8kyHAOoTKBiAX//2/Y8K4Eu0v2oAUpcQoM449Wf/MQLAC1PAFPD1bf/znes/LAACtz6WP9GvzgCQZ/wJ0CGA2gTqAsDLkpAA/Pq75acAYF+VX/lvU0sfd3/d/Hf8RX9fhwDqEigVQODvLgD2+q/0/fnvXP545ff8KQEcOQuYedQEgNekQQH4/g3z3+kA7v9IAHBnHH8JYN++4QPASxI5Fqk/zBW165+qvwrAlL/OX4rPE2Dfvqxb5BrIuZQI4Kjnv9ELIJz/lj6B+sPen/+wZ5S/8PrMT8sE2DlEA8D3p0IBQF0gf5grnPIjAvhXJwBu/7b+V/oSwDOI92Z/cJ4cAL49GTcA9/rHX/9kB2TV3/iH+pH5jwbgAJ5xyYdx/HQgaFMmALgzoX8wAVx/+s3yb6m/QvyJax7ECFKBoE0kAHxzOkfhLrSvf+KvFwC9AYB+rT8HoNrf+BM//H8YQyJQtAgDwLdmYAdQu/xVn/8af25+SiBef5L36687APLERRdddEfPD84LB+D2f+TzD7v/cQIk/Zr1zw1gnwoA7gIFsPtFH8cwkoCkIQgA35iDCcDvf8gT0DcNwOpCfAMU6PvznyH/i3bv3v09OUsBNCsKBwBzhd3/OP/BXsrP6x/3P+T9+kOeaai/sOuX0+cBNCv8APBtWagA/Mc/avd/3Prwt/d/UBfC9gdQF4z/zp0vTF8KIKopGIB//qv1Z6ruh34w/2Ffu/6x/67dEsFO5iV/j9G0AVGNFwC+KQ8dAMwVdf4qANbn+kO+vv5aP9b/pgGE1zyM8bQAVVAsgGD/U6HlI/559Ye6AP9dlf7OnRdc8O6TGFEjUAVuAPiWTCgAT98OQPmj/HoCcATJ/S/2oT8aAPrkf8EF3/g3GFIjkFWUCcDzt+//6ACMvyyB+vafAu5Mav3V9N/9ZdAXf+Il/4RBNQBZhRMAviEXDgDqAO4E7O36c/9L/asA4M5E/EXf879Q7Hfp8l+g/YnXtu+OoSsUCgDiwKq/SkDpo/6Z/g31t+d/xY4drUsBdAU7APxxNkdd/0CfgL+c+uHP6vIb3Blbv67+ZvY701+zY8eOPS/4GEZWB4SZIgHAXKjd/1f9T7+s8tsBpPQ/+6vy76oaAO7Mjj1Cy+YYwowVAP4wHy8A2BPK3vZXAaT2P65/oa4wDRCbAPDfcwRDqwPKROkAwuVft7+yR/2rBODOpNTf+Jv62/5TD8DufyQg/joAySCqL/5VAEo/aABTflN/uDPkjwS6BIA/6kAVwPrIAmj5i73Mfz0B4M7Y5dcBePpm/pv1zy5/NQHaAzAJFA0gMgHgz8s/+6sJAP2U+R/t/13B/ofZW02APbMdAsAfdEEH0Lr+Uwzq6U8VAdyZwD+2AEbq78x/0wDtAVQJFAzAXv+d+3/yASj7Y/qHC0DoXzMBlL+e/1AXjD35TyMAZ/7b+lJ+ngCcgQTQ2d/Vd/z32PXvEgC+3In73fore8IKwC6/JAB1wfaHfbgAhP5WANb0F/+EAHQChQIItj8E9Ln/OYLa/Q8FAHvCDgDqgr7+sc7/kGd8/9EDsPe/mP+ij/5XCyD7t/W/DiD0D9Y/uDPu/O8UAL7YDQ4A8v4EqPzpV9yfA4C8NwHgzsB/V/z6x2qA2fQAkECRALaGAYi+/fyDsbf9n+bUv1mfad7/6frPzowcQLgAwF/g8mMBUBlAngn9w+VP1j+VQNQ/0J+dye4AfKkj90Pe07f8Vf3RAnBnbH0VAD//4gVg6g97AvKEv/wRM2lTQCVQPoBtfv15/xft/6D++3QHQJ2x+h/yzvx3T//CzMzs0ikFoMpvL39cf90ACrgzkf4P/CmAC5EA9J3y0wbAmwAzzLLMAPCFriAApa/vf5jlr2b9c/2xAApQF6r6mxugFsH5XwJYuiwtAEmgXADiTyh/0VcB1PvDnRD90J/7XxbA6P0v+/q3mgCzs08+dToBOCtgdf6T/idzlQHcmZh/dP5zBzTe/2PgzvU/ZdnoHfCAry8NIPL0W3z+UwCQJ6Afmf/++uf6VzMA9uJP+rkB4D87wwHY+lz8KoA6fdMBos/E6q+o6m8C2BtpADr/ncL6qQFwAoUCsK7/N6sAwgUA7oylT1T28fpH2z9y/qPT3yxN/2kEoB//wPxXATD2AwBwJ9i+8j/TXgCgLuj5T0T9/QCo/Sv/0QNQiD7qT2Q8/4cGgDpj+n9X7P5fWH/pfwpAZTCVAERfVn9V/iZ/PwBP3+r/+PoX9X+yuAtZAeA/uqMCEH+7/631r63+QQBV/Y2/FYC1/Jn+p/2fITEASqBYAK4/rv/i1z+OP839wJ+3v0q/9f5PFcDsqXL+E5YsGT0AT59/Wf3vBgB5AsWPLABKv+b8H5sAvP/VLJlWAGr+qwnA819HAHUm7P9QH/3PvyL1t+9/wp79T61OAFMI4MQ3iL89/8U90Gd/yBPKP7r/1Rtg6NfUH/bsD3WB/LMCwL/24jPPhz+2f1b/Q11w6q8XwHj9E9Z/0/+zS50FICOASaEAJpPbn2WdANX+RyKAuhD2PxH6iz4Be8cf6oLWn1k64/b/VAKYnPhjskcCpv61818CYHnXX/W/oPsf6oL1+U+lTxsAdwGcUgC0Fn4duVMApF/jD3dGqq8SgDtj+UOfgDwRm/9m/09Af0oBTCaffJ57+q+tv9IP/CUALn68/+0JAPng/AemFMBk8fbz+PPviL/bACyPCQB1ofKvln87gPgJ0PKvAvi8tdMKgJaChPlvld/3l+6vOf/BndH21vWfXf+1Uwxgkuof7X/lb9a/+PyHP61+0fqvJaYXwCL5qwjgzvjtH+t/tL/tb9c/XP+JpUu90z/D/mvvxXBaefxJ+JdSPJB4/9P3l/qrBGDvLH9W/0OeG8D0/xKn/hkdMCkeQPgAhNP/dgNAXagaIDb/7fY3AdgbYNhr/zXJHVA8gE9K+bvMf/jrBrDmf/D8B0Pnv8gCqPTXrJleB9xe+R8+fDECgDzR4C/64fP/TOjP+z9LP/CfYgf8RVX/w5PFW2P1j9z/rwIw53+4M5Hrf/Ln+38A9rr/OYHpBfAm7T/PPwn5s9fa9cf89/3J/sKm9S9Y/2X7b67/YG/Vf5oBvLGa/+qHPPzn1ZDX9VfAXajmf7T/+QMg+JsAZqz6h/0/3QCgrwOYLB62/REA1BnWr58Ase3vzOzMMrMAwt71X/Ov6uAJlA7g2dCfn/8svjI5/kMmgLi/2Me2P5H+Z+DOwN7zn2IAsCfwBea+N6D+0eUP9Ye+HcBeU37T/7P4/FOAvV7/oP+EC2Ay+cSLYe8FwPa1/tb5D/YETwD+AFz03flv/JdPLYD7YD8//534CljU9a/xj02AiD+f/+rqD3li+RMggHXX4isaXX9vARD7eP33wp7R+vz8QwXsg/pPswP+Q/sHAcTvfyv96PoXvwAK73+SvyQAd2aKHXBY+6+7FV/RhAugCQD6BNwJ6/xnrX/WB2CwJ6T+1gJATDkA3v8exlc0rB+vf2T/t7em/2fMDQDIB/N/ygG8T/svxAKAu0D+F+4i+/j5H+qEsidIv3YDAHWB/acXwA8ggIWFT+MrGlffNIAJwOl/MwGgTwF4n/8p/AYQ/xVTC+Ba6C8s+D/vLeh/HUBgTxPAbP+VP9/+088/KaDv+4v+8hXTC+CrVf/HAoC7oO2t9d8kELn/wdM/pf7Qn2IAlX/ws5+tAEz54ydAuDPKnxOwGwD2deWfZgCVfxAA5AnRlwDMB8BQZ6z21/6kH3z+S/j+pgFyAih6W/x45R8EcA30zfoX1Y+sfzz/I/f/RD9Y/pT/Ux7FUVsp/LnAfdXzf1cHP9hIJ6Db3/K3Eoje/giefxC8BMRe/E+7OemnyQilA9D+Cwsv/ii+pnm91sf0j8//yAaQrn/hzsDe73+r/lcll58oHIC++yO3P97g/thP8bcCiPnHzv/V9p//R8mTfu0CeFn69GeGDODMfe/HlwVP31wAQJ7bP/L3n6j/zfLXWv/TbsHRUhkoANbnBF5tdgOLKgD99z8h79TfvgFU+UcfgBB/a/pzADz/MyY/GDQA3v7/iV4M/8tpANh7/kH/E3T5ozsA9tHlj8t/IGfyg8IBTA5/OfRx+3/fvktwUfCg6Nf7x57/pwTk738A6NdMgKf8izpQHqUDmBx37n/KBYD6Gcgf5/4X/7rzP+QJ6Hv7f9j79Vf+p90sh8+GAiicgLr/afQJ+QHAHzL11wsA1IVo+e3bP83r33XZk19R5klRj8Uf0QEof+Kahyd3BOs/1AX7/GcWgNj1j/hbAbB+l8mvGCQA2g+92vWn5e9Dr1MBRC//6fLHX/+o/Z0GgH7Mf//dOGwHBgpgsvh+PwDS9xZAuBN7rftfWp9Pf+HtL9+f9D8//9RnMVQAk8nD3w11he7/yh/uDOmHKwBd/8CegX5k/buuc/cLEsAwCUw+egnkzfnf/QGYFRF9Z/8D+0j/H8jb9wYU+jtDNRxXlz+Vvln+m59/4+0/7X+qG8Cwhz/cieX778KROjNsAJPJp78F/iqAtvKj/hxA7fNvkGd6TX7F0AFMFt+r1j/lbxrAAHVG//wPOv/VPv8Cd+LKfpNfMXgAtAP+1ioA2BOQ5+2vNQGUPp//Iv6ibwLIvOitAwEMmsDkDu/zX8gLZB/4O/e/oO+tf6f3nvwKdh8+gMnDr5NPgOBvBRB7/k8+/qxrAPjf0H/yK0YKYDL58Isi/U94yz/Dz//VLIDQP1hi8itGC2Cy+GbY2/7h+s8ngFObz3999r0BVQDDJzB5ja9P/kEDONsf428COP1OvF0RRH3cAODO+NsfpuH5P/F/S6nJrxD1sQJ4oetv/fgLE0D0439T/4MP4b1KIeoqgOETcP33hu3P/R/7/F/syf/SQqc+gzIfMQDIM+H2h89/sgCoOfCRz7H8KYEvOFS2+xllPlIAD9b5W/1v3/+bnLzR9n9ruVOfQZkjgKETeBDqQujP5bd+/sOyl9NLHv1R7f/S9EefM4D4FAKIlX/2FGf9f4W86MilrH9G8cmvgPhIAXwQ8oTlXwUwy89/6gSo81UAk8mhM9YOMPkVENcBDJyACcDtf176pQEgT/DUvxEvm5wcYvIL0B4rgHdD3/YXd7InlpknANjfBBChUCTQHiuA74d/+PwP6dPpr1oAxH9J3Y73oVteteIq/HtPoG0CGDYBBBA7/zuffyj/6F/+fvSuV50mz3/gv/sB6dEC+Dbxtz7/FnflH3n8yw/g5N1v2a8e/yD+B1/sBaTtAAZNQPzt/X81Aezrf+gvWeLM87tvuIw//9L+c7nPQESB84gB8MffbgNI+dX+VwH7JWuX4FWTyb2HDtJlkPgjgLm5V+LP+gBlYpwATnL9LZQ+JxC5+0P7P3nRQ3deeTpfBLv+lID8aT+gTFgBDJgAbQSD9udPP539b6W/9qW04r1VyTOWP+nPzfW/IwxhZqQA6v7+M9wJZa+2/2dcihtgjOgjANafm+v4LIQFhBk7gOES+Fj4+TdP/1PMCaD6C2AcgLr/o7D04T/3MrxrZ6ArjBPAB+FOKH1Z/+AuQB8BQJ5Q+nb95172a3jXzkBXcAIYLAETQOVf/fxTAu6M62/ry/r31B/72/5bYcgqxgngN6FPiD6fAYL9LyH6fgDa/7SrbimyB2oKYKgEvg/21gJwSuwGqGPvPP9+4OYy8gRUwTgBvNa2lwkQ+fgrvv7RBLjsXWU+CgVQBV4AAyXwAjsA0p9ZGlsAXH3lv/9d/1D4jghENeMEYPvz/LfqXwUg5Xfqv/+6uwa4HwJRjR/AMAlY/nz+i/z4A9//9CvvLP05iAKaFaMEcIT1VQDS//ELAKN/8NAw8gw0K4IAhkiAA7AaAPIE5Am7/gfxsiGApGGUAP7Z+Mv9Xw3kCdt/2gEMkMBNyp5wnn+EPGHrr1lzA142AFC0iARQPoG/rPTtv/8MecKp/5o1/a/26oCgzSgB/Jayp1+R+39K3/JfPvUAiifw7RIAXwBFbgAG/hl/9z0T6DmMGUC0AUL/J0AApRP4ZtUA1uWfNwEgT/D2d6gAIOcSD6BwAtIAsR//F/VfgVeVBmoe4wTACyBN/mD+iz/cGfFfjleVBmoeNQEUTeC/ZQGEOwH5aPn58h8vKwzEfOoCKJnAEVX/CugHCyACOICXlQVaAbUBFEyAAkh4/l31P9/8wcuKAqmQMQK4ie9/6haAPeH6S/UlgSdKAOUSuAnuAuz99q8WgBUrhtgIQilCQwDFEviduvtfwfQXBggAQjGaAiiVwHfA3tKvWQAkgiIffztAJ8ooAQTz39OX8usGmCu/EYROlMYACiXwTfBPmP/8+VfxACATpzmAMglAX/vX/PwPFcDc3FzpO8FQqaElgCIJwL8KwNXH9Ffwh594VSkgUkdbAAUSeCjUD+pv9EsHAI1aWgPon8ARY8/E/O0AnoqXlQES9bQH0DsBCgDuTNS/mv/EFXhZEaDQQEIAfRPgjSDslT/cGVt/gAAg0ERKAD0TuNHVtwJQ+s4CMDf363hZATD8RkYI4LerADx/ZwGsAii4E8bwG0kKoF8Cr4B+MP9jE2BudbkAMPhm0gLolUAVgOcv8n7951YX2whi6C0kBtAngpdb/nBnpPq+/+rVpQLAsFtJDqB7Akbfr7+3/DGrVw/wIFQT6QF0TsD4w50R/yAAaoDVeFU/MOQEMgLomMCjkfor/aj/KrysFxhwCjkBdEvgSKQB7AAgT7D+6i/Ey/qA4SaRFUCnBCgA0Y/Pf3sB4ABWFdgIYrBp5AXQJYGP8P8BYq2/CUD6v0QAGGoimQF0SOBGKX+dvw4A+qtW/QZe1hkMNJXcAPIjUAFAnrECgL2AAH4XL+sIBplOfgC5CfyKrb/cfv7X7X/R7xsAhphBhwAyEzgY9H/1/LvBBNBrI4gB5tAlgLwIKADIM6b64fon9AgAg8ujWwA5CVwKdYLLrxOoLv8c/VWruu+EMbRMOgaQkQDkGbH3+3+11f8EXpUPBpZL1wDSI4A8IeWPrX+Wf9cAMKh8ugeQmMBDsHf9IU94/p+Ll2WCIXWgRwBpEdwLfdvfzH/ff9XleFkWGE4negWQEoEOwPJ36w9z0CEADKUjPQNoT+BQi78XwK/iZelgIF3pG0BrBCqAqD+v//DWrMzdCGIQ3ekfQEsEN1T+St+qfzgBVmYGgAH0oUQAjRGYv/8fToA5eGtWrlz5d3hZCjh4P8oE0BDBDQcPHrB4pXCFx+VXXA7+Fy9rBwfuS6kAEpbDouCg/SkXwJgR4IAlKBnAWBHgYGUoGwCBQQ4HjlOK4gEMGwEOUZABAiAw3MLgzcsyTAAEBl0MvG1xBguAwNALgDccgiEDICDQC7zVQAwcAAOPTuAtBmSEAAQIZYAXDs1YAQhQawXfPgqjBlABUQ/84Zg8/vj/A4+n+FZ18RiQAAAAAElFTkSuQmCC"
    };

}

//
// IBBuildModule::IronbarkFoundation
//


// Core Base of the Application...
// Provides constants and variables and some handlers and config init/setup
class IBFoundation {

    public static DEBUG_LEVEL: number = 0; //Default debugging level

    /**
     * A function that serves as a debugging tool for the Ironbark server console.
     * This tries to emulate the default console.log() method.
     * @param level Provides the debug level at which the message is printed.
     * @param message The text to be printed if the level parameter is less than or equal to the debug level.
     * 
     * @returns {boolean} Returns whether or not the message was printed.
     */
    public static log ( level: number, message: any, ...optionalParams: any[] ): boolean {
        if ( level <= this.DEBUG_LEVEL ) {
            optionalParams.unshift(message)
            console.log.apply(null, optionalParams);
            return true;
        }
        return false;
    }

    public static readonly mimeTypes: {[key: string]: string} = {
        //'': 'text/plain',
        '.html':'text/html',
        '.htm': 'text/html',
        '.js':  'text/javascript',
        '.css': 'text/css',
        '.json':'application/json',
        '.php': 'text/html',
        '.png': 'image/png',
        '.jpg': 'image/jpeg',
        '.jpeg':'image/jpeg',
        '.gif': 'image/gif',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mpeg3',
        '.mp4': 'video/mp4',
        '.mov': 'video/quicktime',
        '.woff':'application/font-woff',
        '.ttf': 'application/font-ttf',
        '.eot': 'application/vnd.ms-fontobject',
        '.otf': 'application/font-otf',
        '.svg': 'application/image/svg+xml',
        '.md':  'text/markdown'
    };

    public static readonly statusMessages: {[key: number]: string} = {
        400: "Your browser made an error.",
        401: "Your are not authorised to access this page.",
        403: "You are not allowed to access this page.",
        404: "The page you were looking for doesn't exist.",
        500: "Something went wrong on our end."
    };
    
    public static serveErrorPage( code: number = 500, request: HTTP.IncomingMessage, response: HTTP.ServerResponse, onlyHeader: boolean = false) {

        /*function basicError(code:number):string {
            let msg = IronbarkFoundation.errorMessages[code];
            return `<h1 style="font-family: sans-serif; font-size: 3em;">HTTP Error ${String(code)}</h1><hr/><h2 style="font-family: sans-serif; font-size: 2em;">${msg}</h2>`;
        };*/

        function generateErrorPage (code: number): string {
            let msg = IBFoundation.statusMessages[code];
            return `<html>
            <head>
                <title>Ironbark ${String(code)}</title>
                <style>
                    body {
                        font-family: 'Segoe UI', sans-serif;
                        background: #222;
                        text-align: center;
                        position: relative;
                        color: white;
                    }
                    h1 {
                        font-size: 3em;
                        margin: 1em 0;
                    }
                    img {
                        margin: 1em 0;
                        width: 15em;
                        height: 15em;
                        margin-bottom: -1em;
                    }
                </style>
            </head>
            <body>
                <img alt="Ironbark Logo" src="${IBData.websiteIcon.header+IBData.websiteIcon.data}" />
                <h1 style="font-size: 3em;margin: 1em 0;">HTTP ERROR ${String(code)}</h1>
                <hr style="background: white; border: 2px solid white;" />
                <p style="font-size: 1.5em;">${msg}</p>
            </body>
            </html>`.split('\n').join("");
        }

        if (onlyHeader) {
            response.writeHead(code);
            //response.write(generateErrorPage(code), ()=>{
            //    response.end();
            //});
            response.end();
            return;
        } else {
            response.writeHead(code, {"Content-Type": "text/html"});
            response.write(generateErrorPage(code), ()=>{
                response.end();
            });
            return;
        }
        
    }
    

}


//
// IBBuildModule::IronbarkModuleLoader
//


//import myMod = require("../ironbark-modules/ironbark-my1");
const MODULES_PATH: string = "./ironbark-modules/";
var modulesToLoad = FS.readdirSync(MODULES_PATH);
//console.log(modulesToLoad);
//console.log("+", require.resolve.paths("../ironbark-modules/Ironbark.ts"))
var modules: any[] = [];

for (var filename of modulesToLoad) {
    var name: string = filename.split(".").splice(0,filename.split(".").length-1).join("");
    var extension: string = filename.split(".")[filename.split(".").length-1]
    if (extension == "js" && name != "_template") {
        console.log("Loading Module: "+name);
        modules.push(require("../"+MODULES_PATH+name));
    }
}
console.log(modules);
//console.log(FS.readdirSync(MODULES_PATH));
console.log("Module Loader done");

