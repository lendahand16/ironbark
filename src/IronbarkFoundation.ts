
import HTTP = require("http");      // NodeJS HTTP Module
import HTTPS = require("https");    // NodeJS HTTPS Module
import FS = require("fs");          // NodeJS File System Module
import SQLi3 = require("sqlite3");  // NodeJS SQLite3 Module

import IBDataImport = require("./IronbarkData");
export const IBData = IBDataImport.IBData;

// IBBuild::Start

// Core Base of the Application...
// Provides constants and variables and some handlers and config init/setup
// IBBuild::RemoveExport
export class IBFoundation {

    public static DEBUG_LEVEL: number = 0; //Default debugging level

    /**
     * A function that serves as a debugging tool for the Ironbark server console.
     * This tries to emulate the default console.log() method.
     * @param level Provides the debug level at which the message is printed.
     * @param message The text to be printed if the level parameter is less than or equal to the debug level.
     * 
     * @returns {boolean} Returns whether or not the message was printed.
     */
    public static log ( level: number, message: any, ...optionalParams: any[] ): boolean {
        if ( level <= this.DEBUG_LEVEL ) {
            optionalParams.unshift(message)
            console.log.apply(null, optionalParams);
            return true;
        }
        return false;
    }

    public static readonly mimeTypes: {[key: string]: string} = {
        //'': 'text/plain',
        '.html':'text/html',
        '.htm': 'text/html',
        '.js':  'text/javascript',
        '.css': 'text/css',
        '.json':'application/json',
        '.php': 'text/html',
        '.png': 'image/png',
        '.jpg': 'image/jpeg',
        '.jpeg':'image/jpeg',
        '.gif': 'image/gif',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mpeg3',
        '.mp4': 'video/mp4',
        '.mov': 'video/quicktime',
        '.woff':'application/font-woff',
        '.ttf': 'application/font-ttf',
        '.eot': 'application/vnd.ms-fontobject',
        '.otf': 'application/font-otf',
        '.svg': 'application/image/svg+xml',
        '.md':  'text/markdown'
    };

    public static readonly statusMessages: {[key: number]: string} = {
        400: "Your browser made an error.",
        401: "Your are not authorised to access this page.",
        403: "You are not allowed to access this page.",
        404: "The page you were looking for doesn't exist.",
        500: "Something went wrong on our end."
    };
    
    public static serveErrorPage( code: number = 500, request: HTTP.IncomingMessage, response: HTTP.ServerResponse, onlyHeader: boolean = false) {

        /*function basicError(code:number):string {
            let msg = IronbarkFoundation.errorMessages[code];
            return `<h1 style="font-family: sans-serif; font-size: 3em;">HTTP Error ${String(code)}</h1><hr/><h2 style="font-family: sans-serif; font-size: 2em;">${msg}</h2>`;
        };*/

        function generateErrorPage (code: number): string {
            let msg = IBFoundation.statusMessages[code];
            return `<html>
            <head>
                <title>Ironbark ${String(code)}</title>
                <style>
                    body {
                        font-family: 'Segoe UI', sans-serif;
                        background: #222;
                        text-align: center;
                        position: relative;
                        color: white;
                    }
                    h1 {
                        font-size: 3em;
                        margin: 1em 0;
                    }
                    img {
                        margin: 1em 0;
                        width: 15em;
                        height: 15em;
                        margin-bottom: -1em;
                    }
                </style>
            </head>
            <body>
                <img alt="Ironbark Logo" src="${IBData.websiteIcon.header+IBData.websiteIcon.data}" />
                <h1 style="font-size: 3em;margin: 1em 0;">HTTP ERROR ${String(code)}</h1>
                <hr style="background: white; border: 2px solid white;" />
                <p style="font-size: 1.5em;">${msg}</p>
            </body>
            </html>`.split('\n').join("");
        }

        if (onlyHeader) {
            response.writeHead(code);
            //response.write(generateErrorPage(code), ()=>{
            //    response.end();
            //});
            response.end();
            return;
        } else {
            response.writeHead(code, {"Content-Type": "text/html"});
            response.write(generateErrorPage(code), ()=>{
                response.end();
            });
            return;
        }
        
    }
    

}

// IBBuild::End
