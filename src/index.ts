//@ts-check

/*
  Index file included as main launch script for Application
*/

import Ironbark = require("./Ironbark");

const PORT: number = Number(process.env.PORT) || 80;
Ironbark.IBFoundation.DEBUG_LEVEL = 2;
//var server = new Ironbark.IronbarkHTTPServer( PORT );
// need to input domain/hostname so subdomains can be used
var server = new Ironbark.Trunk( PORT );
server.start();
//server.stop();

// var trunk = new Ironbark.Trunk -> Root Server e.g. computer.com
// var bark = new Ironbark.Bark   -> Extension App e.g. myapp.computer.com
// bark servers are registered with the trunk at its creation
// new Ironbark.Trunk( PORT, {barkServers: [bark1, bark2]} )
/*
PLANNED

import Ironbark = require("./Ironbark");

const PORT: number = Number(process.env.port) || 80;
const SERVER = new Ironbark.IBServer(PORT: number, DEBUG_LEVEL: number | string);
SERVER.begin();
*/