//@ts-check

console.log("Module Loader beginning");
import FS = require("fs");

// IBBuild::Start

//import myMod = require("../ironbark-modules/ironbark-my1");
const MODULES_PATH: string = "./ironbark-modules/";
var modulesToLoad = FS.readdirSync(MODULES_PATH);
//console.log(modulesToLoad);
//console.log("+", require.resolve.paths("../ironbark-modules/Ironbark.ts"))
var modules: any[] = [];

for (var filename of modulesToLoad) {
    var name: string = filename.split(".").splice(0,filename.split(".").length-1).join("");
    var extension: string = filename.split(".")[filename.split(".").length-1]
    if (extension == "js" && name != "_template") {
        console.log("Loading Module: "+name);
        modules.push(require("../"+MODULES_PATH+name));
    }
}
console.log(modules);
//console.log(FS.readdirSync(MODULES_PATH));
console.log("Module Loader done");

// IBBuild::End
